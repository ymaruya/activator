<?php

/**
 * MemberLog filter form base class.
 *
 * @package    sensingtechnology
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMemberLogFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'member_info_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MemberInfo'), 'add_empty' => true)),
      'latitude'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'longitude'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'x'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'y'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'receive_status' => new sfWidgetFormFilterInput(),
      'is_init'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'v'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ax'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ay'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'az'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'gps_time'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'distance'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'member_info_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('MemberInfo'), 'column' => 'id')),
      'latitude'       => new sfValidatorPass(array('required' => false)),
      'longitude'      => new sfValidatorPass(array('required' => false)),
      'x'              => new sfValidatorPass(array('required' => false)),
      'y'              => new sfValidatorPass(array('required' => false)),
      'receive_status' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_init'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'v'              => new sfValidatorPass(array('required' => false)),
      'ax'             => new sfValidatorPass(array('required' => false)),
      'ay'             => new sfValidatorPass(array('required' => false)),
      'az'             => new sfValidatorPass(array('required' => false)),
      'gps_time'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'distance'       => new sfValidatorPass(array('required' => false)),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('member_log_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MemberLog';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'member_info_id' => 'ForeignKey',
      'latitude'       => 'Text',
      'longitude'      => 'Text',
      'x'              => 'Text',
      'y'              => 'Text',
      'receive_status' => 'Number',
      'is_init'        => 'Boolean',
      'v'              => 'Text',
      'ax'             => 'Text',
      'ay'             => 'Text',
      'az'             => 'Text',
      'gps_time'       => 'Date',
      'distance'       => 'Text',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
    );
  }
}
