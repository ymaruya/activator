<?php

/**
 * MemberRequestLog form base class.
 *
 * @method MemberRequestLog getObject() Returns the current form's model object
 *
 * @package    sensingtechnology
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMemberRequestLogForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                    => new sfWidgetFormInputHidden(),
      'member_info_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MemberInfo'), 'add_empty' => false)),
      'latitude'              => new sfWidgetFormInputText(),
      'longitude'             => new sfWidgetFormInputText(),
      'serach_range'          => new sfWidgetFormInputText(),
      'serach_datetime'       => new sfWidgetFormInputText(),
      'serach_datetime_range' => new sfWidgetFormInputText(),
      'created_at'            => new sfWidgetFormDateTime(),
      'updated_at'            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'member_info_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MemberInfo'))),
      'latitude'              => new sfValidatorString(array('max_length' => 255)),
      'longitude'             => new sfValidatorString(array('max_length' => 255)),
      'serach_range'          => new sfValidatorString(array('max_length' => 255)),
      'serach_datetime'       => new sfValidatorString(array('max_length' => 255)),
      'serach_datetime_range' => new sfValidatorString(array('max_length' => 255)),
      'created_at'            => new sfValidatorDateTime(),
      'updated_at'            => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('member_request_log[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MemberRequestLog';
  }

}
