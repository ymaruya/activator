<?php

/**
 * MemberLog form base class.
 *
 * @method MemberLog getObject() Returns the current form's model object
 *
 * @package    sensingtechnology
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseMemberLogForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'member_info_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('MemberInfo'), 'add_empty' => false)),
      'latitude'       => new sfWidgetFormInputText(),
      'longitude'      => new sfWidgetFormInputText(),
      'x'              => new sfWidgetFormInputText(),
      'y'              => new sfWidgetFormInputText(),
      'receive_status' => new sfWidgetFormInputText(),
      'is_init'        => new sfWidgetFormInputCheckbox(),
      'v'              => new sfWidgetFormInputText(),
      'ax'             => new sfWidgetFormInputText(),
      'ay'             => new sfWidgetFormInputText(),
      'az'             => new sfWidgetFormInputText(),
      'gps_time'       => new sfWidgetFormInputText(),
      'distance'       => new sfWidgetFormInputText(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'member_info_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('MemberInfo'))),
      'latitude'       => new sfValidatorString(array('max_length' => 255)),
      'longitude'      => new sfValidatorString(array('max_length' => 255)),
      'x'              => new sfValidatorString(array('max_length' => 255)),
      'y'              => new sfValidatorString(array('max_length' => 255)),
      'receive_status' => new sfValidatorInteger(array('required' => false)),
      'is_init'        => new sfValidatorBoolean(array('required' => false)),
      'v'              => new sfValidatorString(array('max_length' => 255)),
      'ax'             => new sfValidatorString(array('max_length' => 255)),
      'ay'             => new sfValidatorString(array('max_length' => 255)),
      'az'             => new sfValidatorString(array('max_length' => 255)),
      'gps_time'       => new sfValidatorPass(),
      'distance'       => new sfValidatorString(array('max_length' => 255)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('member_log[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'MemberLog';
  }

}
