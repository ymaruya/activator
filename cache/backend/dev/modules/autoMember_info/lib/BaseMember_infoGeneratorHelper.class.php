<?php

/**
 * member_info module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage member_info
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: helper.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class BaseMember_infoGeneratorHelper extends sfModelGeneratorHelper
{
  public function getUrlForAction($action)
  {
    return 'list' == $action ? 'member_info' : 'member_info_'.$action;
  }
}
