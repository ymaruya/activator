<?php use_helper('I18N', 'Date') ?>
<?php include_partial('member_info/assets') ?>

<div id="sf_admin_container">
  <h1><?php echo __('Edit Member info', array(), 'messages') ?></h1>

  <?php include_partial('member_info/flashes') ?>

  <div id="sf_admin_header">
    <?php include_partial('member_info/form_header', array('member_info' => $member_info, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>

  <div id="sf_admin_content">
    <?php include_partial('member_info/form', array('member_info' => $member_info, 'form' => $form, 'configuration' => $configuration, 'helper' => $helper)) ?>
  </div>

  <div id="sf_admin_footer">
    <?php include_partial('member_info/form_footer', array('member_info' => $member_info, 'form' => $form, 'configuration' => $configuration)) ?>
  </div>
</div>
