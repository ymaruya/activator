<td class="sf_admin_text sf_admin_list_td_id">
  <?php echo link_to($member_info->getId(), 'member_info_edit', $member_info) ?>
</td>
<td class="sf_admin_text sf_admin_list_td_username">
  <?php echo $member_info->getUsername() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_birthday">
  <?php echo false !== strtotime($member_info->getBirthday()) ? format_date($member_info->getBirthday(), "f") : '&nbsp;' ?>
</td>
<td class="sf_admin_text sf_admin_list_td_sex">
  <?php echo $member_info->getSex() ?>
</td>
<td class="sf_admin_date sf_admin_list_td_created_at">
  <?php echo false !== strtotime($member_info->getCreatedAt()) ? format_date($member_info->getCreatedAt(), "f") : '&nbsp;' ?>
</td>
