<td colspan="5">
  <?php echo __('%%id%% - %%username%% - %%birthday%% - %%sex%% - %%created_at%%', array('%%id%%' => link_to($member_info->getId(), 'member_info_edit', $member_info), '%%username%%' => $member_info->getUsername(), '%%birthday%%' => false !== strtotime($member_info->getBirthday()) ? format_date($member_info->getBirthday(), "f") : '&nbsp;', '%%sex%%' => $member_info->getSex(), '%%created_at%%' => false !== strtotime($member_info->getCreatedAt()) ? format_date($member_info->getCreatedAt(), "f") : '&nbsp;'), 'messages') ?>
</td>
