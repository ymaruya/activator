/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*参考
 * GPS
 * http://www.atmarkit.co.jp/fdotnet/chushin/introhtml5_01/introhtml5_01_01.html
 * http://www6.ocn.ne.jp/~wot/web/html5/geoapi/wp.html
 * http://gihyo.jp/dev/feature/01/location-based-services/0003
 * http://www.extjs.co.jp/blog/2010/10/15/the-html5-family-geolocation/
 * 加速度
 * http://ameblo.jp/chicktack123/entry-11299544291.html
 * http://d.hatena.ne.jp/moto_maka/20120604/1338752269
 */

var m = 1000;
//grobal
var lat = 0;
var lng = 0;
var v = 0;
var receive_status = 2;
var x = -1;
var y = -1;
var z = -1;
var time = -1;
var is_init = true;


window.onload = function(){
    
    //加速度
    
        window.addEventListener("devicemotion",function(evt){
            x = evt.accelerationIncludingGravity.x; //横方向の傾斜
            y = evt.accelerationIncludingGravity.y; //縦方法の傾斜
            z = evt.accelerationIncludingGravity.z; //上下方向の傾斜
	    },true);
   
          
  timeLoop();
} 

function timeLoop(){
    requestAjax();
}



//success
function gpsReceiveSuccess(position){ 
    receive_status = 0;
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    time = js_yyyy_mm_dd_hh_mm_ss().toString();
    v = parseInt(position.coords.speed) / 1000 * 3600; // m/s = km/h
    if(position.coords.accuracy > 100) receive_status = 1;
    requestAjax();
}


var post_num = 0;

//post
//receive_status : 0 => 良好, 1=>不良, 2=>不可
function requestAjax(){
    
        //この部分でデータを送信している
        //上5つはGPS情報
        //下3つは加速度の情報
        $.ajax({
            type: "POST",
            url: url["logger"],                      // リクエストURL
            data: 
            "id="+id
            +"&lat="+lat 
            +"&lng="+lng
            +"&v="+v
            +"&receice_status="+receive_status
            +"&gps_time="+time
            +"&x="+x
            +"&y="+y
            +"&z="+z
            +"&is_init="+is_init
            , // データ
            success: function(data, status) {
                // 通信成功時にデータを表示
                post_num++;
                is_init = false;
                if(post_num > 1) myGPSAndAccelerationSuccess(lat,lng,data["v"],data["dis"],receive_status,x,y,z);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown){
                // 通常はここでtextStatusやerrorThrownの値を見て処理を切り分けるか、
                // 単純に通信に失敗した際の処理を記述します。
                this; // thisは他のコールバック関数同様にAJAX通信時のオプションを示します。
            },
            dataType: "json"                                 // 応答データ形式
        });

	myGPSAndAccelerationSuccess(lat,lng,0,0,receive_status,x,y,z);

    setTimeout("timeLoop()",gps_receive_interval * m); 
}

function js_yyyy_mm_dd_hh_mm_ss () {
    now = new Date();
    year = "" + now.getFullYear();
    month = "" + (now.getMonth() + 1);
    if (month.length == 1) {
        month = "0" + month;
    }
    day = "" + now.getDate();
    if (day.length == 1) {
        day = "0" + day;
    }
    hour = "" + now.getHours();
    if (hour.length == 1) {
        hour = "0" + hour;
    }
    minute = "" + now.getMinutes();
    if (minute.length == 1) {
        minute = "0" + minute;
    }
    second = "" + now.getSeconds();
    if (second.length == 1) {
        second = "0" + second;
    }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}