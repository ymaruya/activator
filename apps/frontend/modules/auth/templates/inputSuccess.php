

<!-- template -->
<!-- InstanceBeginEditable name="content" -->
<div id="content" class="structre">
  <form action="<?php echo url_for($sf_context->getModuleName()."/input"); ?>" method="post">
    <?php echo $form->renderHiddenFields(); ?>
    <?php if($form->hasErrors()): ?>
    <div class="errorBox clearfix">
      <ul>
        <?php
           foreach ($form->getErrorSchema() as $key => $err) {
        echo "<li>" . $err . "</li>\n";
        }
        ?>
      </ul>
      <!-- / .errorBox clearfix -->
    </div>
    <?php endif; ?>
    <div class="section structre">
       <h1>ログインモジュール</h1>
      <table>
        <tr>
          <th>username</th>
          <td class="cell1"><?php echo $form['username']; ?>
          <th>目的</th>
          <td class="cell1"><?php echo $form['purpose']; ?>
          </td>
        </tr>
      </table>
      <div class="btnLogin">
        <input type="hidden" name="submit" value="送る" />
        <input type="image" name="submit" src="/images/frontend/login.png" />
        <!-- //btnLogin -->
      </div>
    </div>

  </form>
  <!-- //structre -->
  <!-- /#content -->
</div>
<!-- InstanceEndEditable --> 
<!-- /#container --> 
