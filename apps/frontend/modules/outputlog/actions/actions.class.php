<?php

/**
 * outputlog actions.
 *
 * @package    sensingtechnology
 * @subpackage outputlog
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class outputlogActions extends sfActions {

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    //無視
    public function executeIndex(sfWebRequest $request) {
        $this->forward('default', 'module');
    }

    //一覧画面を出力
    public function executeList(sfWebRequest $request) {
        //logが存在するならば
        $this->pager = null;
        $this->member_info = $this->getUser()->getMemberInfo();

        if (MemberLogTable::getInstance()->findByMemberInfoId($this->member_info->getId())) {
            $this->pager = new sfDoctrinePager(
                            'MemberLog',
                            50
            );
            
            $this->pager->setQuery($this->member_info->getMemberLogsQuery());
            $this->pager->setPage($request->getParameter('page', 1));
            $this->pager->init();
        }
    }
    
    
    //検索するデータをセット
    public function executeSearch(sfWebRequest $request){
         //form
        $this->form = new searchForm();
        $this->form->getWidgetSchema()->setNameFormat(searchForm::NAME . '[%s]');
        
        if ($request->isMethod('post')) {
            $values = $request->getParameter($this->form->getName());
            $this->form->bind($values);
            if ($this->form->isValid()) {
                $this->getUser()->setFlash("info", $this->form->getValues());
                if(!$values["output"]) $this->redirect('outputlog/googlemap');
                else  $this->redirect('outputlog/DlList');
            }
        }
    }


    //あたら得られた情報をもとに、googlemapで表示
    public function executeGooglemap(sfWebRequest $request) {
        
        $values = $this->getUser()->getFlash("info");
        if(!$values) $this->redirect('outputlog/search');

        //表示に必要なデータが入っている
        //googlemapSuccessで使う
        $this->member_logs = MemberLogTable::getInstance()->getMemberLogsByInfo($this->getUser()->getMemberInfo(),$values,false);          
      
        //ユーザがいつ見たか
        $member_request_log = new MemberRequestLog();
        $member_request_log->setMemberInfoId($this->getUser()->getMemberInfo()->getId());
        $member_request_log->save();
    }

    /**
     * executeDlList
     *
     * @param sfWebRequest $request
     */
    //csvに出力
    public function executeDlList(sfWebRequest $request) {
        
        
        $values = $this->getUser()->getFlash("info");
        if(!$values) $this->redirect('outputlog/search');
        
        // --------------
        //   initialize
        // --------------
        $user_agent = $request->getHttpHeader('USER_AGENT');
        $return_code = preg_match("/^.+Mac OS.+$/", $user_agent) ? "\r" : "\n";
         // --------------
        //   get record
        // --------------
        
        //表示に必要なデータが入っている
        $member_logs = MemberLogTable::getInstance()->getMemberLogsByInfo($this->getUser()->getMemberInfo(),$values,true);
        
        
        // ------------
        //   make csv
        // ------------
        $list = array();
        //データを取ってくる、保存
        foreach ($member_logs as $member_log) {
            $list[] = array(
                "ユーザー名" => MemberInfoTable::getInstance()->findOneById($member_log->getMemberInfoId())->getUsername(),
		//       "緯度" => $member_log->getLatitude(),
                //"経度" => $member_log->getLongitude(),
                //"緯度(xy座標系)" => $member_log->getX(),
                //"経度(xy座標系)" => $member_log->getY(),
                //"速度(km/h)" => $member_log->getV(),
                "加速度(x)" => $member_log->getAx(),
                "加速度(y)" => $member_log->getAy(),
                "加速度(z)" => $member_log->getAz(),
                "アクセス日時" => strtotime($member_log->getCreatedAt())
            );
        }

        //以下は気にしない
        $csv = null;
        if (count($list)) {
            // header
            $tmp = sprintf('"%s"', implode('","', array_keys($list[0])));
            $data[] = mb_convert_encoding($tmp, "sjis-win", sfConfig::get('sf_charset'));
            // body
            foreach ($list as $row) {
                $tmp = sprintf('"%s"', implode('","', array_values($row)));
                $data[] = mb_convert_encoding($tmp, "sjis-win", sfConfig::get('sf_charset'));
            }
            $csv = implode($return_code, $data);
        }

        // ----------
        //   output
        // ----------
        $file_name = sprintf("fcreator_users_%s.csv", date("YmdHis"));
        $this->getResponse()->clearHttpHeaders();
        $this->getResponse()->addCacheControlHttpHeader("Cache-control", "private");
        $this->getResponse()->setHttpHeader("Content-Description", "File Transfer");
        $this->getResponse()->setContentType('application/octet-stream', TRUE);
        $this->getResponse()->setHttpHeader('Pragma', "public");
        $this->getResponse()->setHttpHeader('Content-transfer-encoding', 'binary', TRUE);
        $this->getResponse()->setHttpHeader("Content-Disposition", "attachment; filename=" . $file_name, TRUE);
        $this->getResponse()->sendHttpHeaders();

        return $this->renderText($csv);
    }

}
