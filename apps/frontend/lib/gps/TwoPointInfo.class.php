<?php
/*2点間の距離を求める(x,y)*/

class TwoPointInfo{
    private static $instance = null;

    private $e1 = 0.08181919104281579; //第一離心率 
    private $a = 6378137; //長半径(m)
    private $m0 = 0.9999;
    
    
    public static function getInsctance(){
        if(is_null(self::$instance)){
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    public function getDistance($x1,$y1,$x2,$y2){
        //場所によって違うみたい
        $ram0 = array("lat" => $this->DEG_TO_RAD(36.0) , "lon" => $this->DEG_TO_RAD(139.83)); //λ0
        $r0 = $this->a * sqrt(1 - pow($this->e1,2)) / (1 - pow($this->e1,2) * pow(sin($ram0["lat"]),2));   
        $sS = $this->m0 * (1 + 1 / (6*pow($r0,2)*pow($this->m0,2)) * (pow($y1,2) + $y1*$y2 + pow($y2,2)));
        
        return sqrt(pow($x2 - $x1,2) + pow($y2 - $y1,2)) / $sS;
    }       
    
     private function DEG_TO_RAD($p){
        return $p * pi() / 180;
    }
}

?>
