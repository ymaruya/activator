<?php

class myUser extends sfBasicSecurityUser
{
    private $is_login = false;
    private $memberInfo;
    private $ns = 'login';
    
    
    /**
     * isLogin
     *
     * @return bool
     */
    public function isLogin() {
        return $this->is_login;
    }

    /**
     * getMemberInfo
     *
     * @return <type>
     */
    public function getMemberInfo() {
        return $this->memberInfo;
    }

    /**
     * doLogin
     *
     * @param MemberInfo $memberInfo
     * @param <type> $isset_remember_key
     */
    public function doLogin(MemberInfo $memberInfo) {
        // ------------------
        //   set attributes
        // ------------------
        $this->setAuthenticated(true);
        $this->addCredential(sfConfig::get('sf_app'));
        $this->setAttribute('member_id', $memberInfo->getId(), $this->ns);


        // -----------------
        //   save UserInfo
        // -----------------
        $request = sfContext::getInstance()->getRequest();
        $this->is_login = true;
        $this->memberInfo = $memberInfo;
    }

    /**
     * ログアウト処理
     */
    public function doLogout() {
        $this->getAttributeHolder()->removeNamespace($this->ns);
        $this->clearCredentials();
        $this->setAuthenticated(false);
        $this->is_login = false;
    }

     /**
     * LoginCheck
     * ※for filterChain
     *
     * @return bool
     */
    public function LoginCheck() {

        $this->is_login = false;
        $user_id = $this->getAttribute('member_id', null, $this->ns);

        if($user_id) $this->memberInfo = Doctrine_core::getTable('MemberInfo')->findOneById($user_id);
        
        if ($this->memberInfo) {
            $this->is_login = true;
        }else{
            $this->doLogout();
        }
        
        return $this->is_login;
    }
}
